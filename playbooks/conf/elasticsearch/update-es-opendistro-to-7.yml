---
# Setup the desired settings as host_vars or group_vars files as needed.
# View the default vars and roles' README for more information.

- name: Updating Elasticsearh and Opendistro
  hosts: elasticsearch-nodes
  become: true
  gather_facts: true
  serial: 1

  vars:
    elasticsearch_pkg_major_version: "7"

  pre_tasks:
    - name: Making directory to store backup configuration
      file:
        path: /tmp/opendistro_backup
        state: directory
        owner: root
        group: root
        mode: '0700'

    - name: Making backup of Opendistro configuration.
      command: "{{ opendistro_elastic_security_admin_script_path }} -r -cd /tmp/opendistro_backup -icl -nhnv -cacert {{ opendistro_elastic_security_admin_cert_pemtrustedcas_filepath }} -cert {{ opendistro_elastic_security_admin_cert_pemcert_filepath }} -key {{ opendistro_elastic_security_admin_cert_pemkey_filepath }}"

    - name: Check that backups files were created
      pause:
        prompt: 'Please confirm Opendistro backups files were created in /tmp/opendistro_backup. The next step will remove the index! Press return to continue. Press Ctrl+c and then "a" to abort'

    - name: Removing opendistro security index from elasticsearch.
      command: "{{ opendistro_elastic_security_admin_script_path }} -dci -icl -nhnv -cacert {{ opendistro_elastic_security_admin_cert_pemtrustedcas_filepath }} -cert {{ opendistro_elastic_security_admin_cert_pemcert_filepath }} -key {{ opendistro_elastic_security_admin_cert_pemkey_filepath }}"

    - name: Stopping Elasticsearch Service
      service:
        name: elasticsearch
        state: stopped

    - name: Removing Elasticsearch Opendistro Packages Before Updating
      package:
        name: "{{ item }}"
        state: removed
      loop:
        - opendistro-alerting
        - opendistro-job-scheduler
        - opendistro-performance-analyzer
        - opendistro-security
        - opendistro-sql
        - opendistroforelasticsearch

    - name: Removing Elasticsearch Package
      package:
        name: elasticsearch-oss
        state: removed

    - name: Removing elastic 6.x repository
      yum_repository:
        name: elastic-oss
        description: Elastic YUM repo
        baseurl: "{{ elastic_yum_repo_url }}"
        state: absent

    - name: Make sure elasticsearch group exists
      group:
        name: "{{ elasticsearch_group }}"
        state: present
        system: true

    - name: Make sure elasticsearch user exists
      user:
        name: "{{ elasticsearch_user }}"
        group: "{{ elasticsearch_group }}"
        home: "/nonexistent"
        create_home: false
        shell: "/sbin/nologin"
        state: present
        system: true

    - name: Make sure folders have the proper permissions.
      file:
        path: "{{ item }}"
        owner: "{{ elasticsearch_user }}"
        group: "{{ elasticsearch_group }}"
        recurse: true
      loop:
        - "{{  elasticsearch_config_path_dir }}"
        - "{{ elasticsearch_config_path_data_dir }}"
        - "{{ elasticsearch_config_path_log_dir }}"

  roles:
    - elasticsearch
    - { role: grid-certs, when: "install_gridcert"}
    - { role: grid-cas, when: "install_grid_cas"}
    - { role: fetch-crl, when: "fetch_crl_needed"}
    - { role: firewalld, when: "firewalld_enabled"}

  post_tasks:
    # - name: Restarting service
    #   service:
    #     name: elasticsearch
    #     state: restarted

    - name: Check that ES is online
      pause:
        prompt: 'Please confirm Elasticsearch has started properly! Press return to continue. Press Ctrl+c and then "a" to abort'

    # - name: Wait for port 9300 to become open on the host (10s delay)
    #   wait_for:
    #     port: 9300
    #     delay: 10
    #
    # - name: Migrating Opendistro Configuration from Backup
    #   command: "{{ opendistro_elastic_security_admin_script_path }} -migrate -cd /tmp/opendistro_backup -icl -nhnv -cacert {{ opendistro_elastic_security_admin_cert_pemtrustedcas_filepath }} -cert {{ opendistro_elastic_security_admin_cert_pemcert_filepath }} -key {{ opendistro_elastic_security_admin_cert_pemkey_filepath }} -icl"
