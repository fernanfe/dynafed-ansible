# Dynafed - Ansible
## Status
The following roles are not complete and/or tested:


## DESCRIPTION

This project is meant to provide all the necessary Ansible roles to install a working [DynaFed](http://lcgdm.web.cern.ch/dynafed-dynamic-federation-project) environment. It uses the following for the different components:

### DynaFed
* Apache
* Memcached
* UGR
* DMLite

### Monitoring/Accounting
* Prometheus
    - Server and Service Metrics
* Elasticsearch
    - Database to store porcessed logs.
* Elastic Filebeat
    - Gathers system logs, sends them to logstash for processing.
* Elastic Execbeat (Community Beat)
    - Can be configured to run any script periodically and send the stdout to logstash for processing.
* Elastic Metricbeat
    - Gathers metrics of the system and services according to which modules are enabled, with the information sent to logstash for processing.
* Logstash
    - Processes logs into useful JSON files for the elsasticsearch db.
* Grafana
    - Visualization/Alerting of information from Pormetheus and elasticsearch.

### Authentication
* Grid Canada certificates and CA's
* VOMS [Grid-mapfile](http://heprc.blogspot.ca/2017/06/grid-mapfile-based-authentication-for.html)
* Fetch-CRL

### Load Balancing
* HAProxy as front end for Elasticsearch and Logstash taking care of SSL encyption.

## Usage
### Installing Dynafed
1. Read README.md files for the [dynafed](roles/dynafed/README.md), [apache](roles/apache/README.md) and [grid-certs](roles/grid-certs/README.md) roles for information on which variables to use for configuration as well as setting up secret files. The other roles used work with the defaults, but if changes need to be made, please check their respective README.md's.
2. Create group_vars or host_vars files with the desired configuration
3. Run playbook "playbooks/conf/dynafed.yml"

If no errors occur, then Apache/DynaFed should be now accessible at the port desigated via var: apache_dynafed_listen_port

## Logs and Monitoring
To keep an accounting of the transactions that DynaFed serves, as well as flexibility to analyze the data in more detail, the elastic ELK stack has been chosen to obtain, process and store the data. Currently Grafana is being utilized to visualize the data.

This is workflow:
Apache --> Filebeat, Execbeat, Metricbeat --> Logstash --> Elasticsearch <-- Grafana

### Configuration
All this configuration is possible through each of the ansible roles in this tree. Should be a matter of setting up the proper variables and a playbook to go with them. (Execpt step 3, coming soon)

1.- [Apache](roles/apache) - [Dynafed](roles/dynafed): Custom logs format for both AccessLogs (dynafed_access_log) and ErrorLogs (dynafed_error_log) need to be setup for the Vhost serving Dynafed (in zlcgdm-ugr-dav.conf).

```apache
LogLevel    warn
LogLevel    lcgdm_ns:info
ErrorLogFormat "[%-{cu}t] [LogID \"%-L\"] [thread \"%-T\"] [client \"%-a\"] [agent \"%-{User-Agent}i\"] [%-M]"
ErrorLog    logs/dynafed_log
CustomLog   logs/dynafed_log "[%{%Y-%m-%d %H:%M:%S}t.%{begin:usec_frac}t] [LogID \"%L\"] [thread %{tid}P] [client %h:%{remote}p] [request \"%r\"] [method %m] [content-length %{Content-Length}i] [query \"%q\"] [urlpath \"%U\"] [status %>s] [agent \"%{User-Agent}i\"]" env=!dontlog
CustomLog   logs/dynafed_ssl_request_log "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"
```
And to enable the /server-status page to obtain Apache metrics, (/etc/httpd/conf.d/server-status.conf) Note that there is no logging when accessing this page thanks to the env=!dontlog parameters in the Log options:

```apache
Listen "localhost:8088"
<VirtualHost localhost:8088>
  <IfModule mod_status.c>
    # This directive works with the CustomLog format to avoid queries to the status page from being logged
    SetEnvIf Request_URI "/server-status" dontlog
    CustomLog "logs/access_log" combined env=!dontlog
    <Location "/server-status">
      SetHandler server-status
      Require host localhost
    </Location>
  </IfModule>
</VirtualHost>

```
Configure Apache to use mpm_events in (/etc/httpd/conf.modules.d/00-mpm.conf) by leaving only the following line uncommented:

```apache
LoadModule mpm_event_module modules/mod_mpm_event.so
```

2.- [Filebeat](roles/filebeat): Running on same server as Apache. It monitors the dynafed_*_log files created by the configuration above and sends them to Logstash injecting tags necessary for their processing:
filebeat.yml

```yaml
## General Options ###
name: "{hostname}"
tags: ["filebeat"]

fields:
  env: production
  dynafedendpoint: "{name of your endpoint}" #If not entered, hostname will be used.

fields_under_root: false
queue:
  mem:
    events: 4096
    flush:
      min_events: 2048
      timeout: 1s

### Filebeats Modules ###
filebeat:
##### Filebeat Module Options #####
  config.modules:
    path: /etc/filebeat/modules.d/*.yml
    reload.enabled: false
    #reload.period: 10s

##### Filebeat Prospector Options #####
## As of Beats 6.3 "prospectors" has been renamed to "inputs". Both work though.
  inputs:
    - type: "log"
      tags: ["dynafed", "httpd", "logs"]
      paths:
        - "/var/log/httpd/dynafed_log"

##### Filebeat Processors Options #####
processors:
  - add_locale: ~

### Outputs ###
output:
  logstash:
    hosts:
      - "logstash-host:5044"
    index: "filebeat"
    ssl:
      enabled: true
      verification_mode: full
      certificate_authorities: ["/etc/ssl/certs/ca-bundle.crt"]

### Paths Options ###
path:
  home:   "/usr/share/filebeat"
  config: "/etc/filebeat"
  data:   "/var/lib/filebeat"
  logs:   "/var/log/filebeat"

### Logging Options ###
logging:
  level: "warning"
  to_files: true
  to_syslog: false
  files:
    path: "/var/log/filebeat"
    name: "filebeat.log"
    keepfiles: 7

```

3.- [dynafed-storagestats](https://github.com/hep-gc/dynafed_storagestats): Running in the same server as Apache/Dynafed install the following python package to pool and retrieve storage stats information from the storage endpoints and upload it to Memcache. (See the README at the link for more information about how this works and is setup):

```bash
pip3 install dynafed-storagestats
```
Set cron to run it periodically, say every 30 minutes (or as needed):

```bash
crontab -e
*/30 * * * * dynafed-storage stats -m --logfile /var/log/dynafed_storagestats/dynafed_storagestats.log
```

4.- [Execbeat](roles/execbeat): Running in the same server as Memcached setup alongside a python script [getugrpluginstats.py](roles/execbeat/files/commands/getugrpluginstats.py) to pull from Memcached the polling information for the Storage Endpoints behind Dynafed. Same information when accessing the /dashboard. Assuming the scripts are placed under /etc/execbeat/commands :

```yaml
name: "{hostname}"
tags: ["execbeat", "dynafed", "metrics", "ugr"]
fields:
  env: production
  dynafedendpoint: "{name of your endpoint}" #If not entered, hostname will be used.

fields_under_root: "false"
queue_size: 1000
max_procs: 1

execbeat:
  commands:
    - command: "/etc/execbeat/commands/getugrpluginstats.py"
      schedule: ""
      document_type: "doc"

output:
  logstash:
      enabled: true
      hosts:
        - "logstash-host:5044"
      index: 'execbeat'
      ssl:
        enabled: true
        verification_mode: full
        certificate_authorities: ["/etc/ssl/certs/ca-bundle.crt"]

path:
  home:   "/usr/share/execbeat"
  config: "/etc/execbeat"
  data:   "/var/lib/execbeat"
  logs:   "/var/log/execbeat"


logging:
  level: warning
  to_syslog: false
  metrics:
    enabled: false
    period: 30s
  to_files: true
  files:
    path: "/var/log/execbeat"
    name: execbeat.log
    rotateeverybytes: 104857600 #100MB
    keepfiles: 7
```

5.- [Metricbeat](roles/metricbeat): Running in the same server as Apache. Reads the /server-status page created created by Apache as setup on step 1, and exports this information to logstash:

```yaml
### General Options ###
name: "{hostname}"
tags: ["metricbeat"]
fields:
  env: production
  dynafedendpoint: "{name of your endpoint}" #If not entered, hostname will be used.

fields_under_root: False
queue:
  mem:
    events: 4096
max_procs: 1
metricbeat.max_start_delay: 10s

### Paths Options ###
path:
  home:   "/usr/share/metricbeat"
  config: "/etc/metricbeat"
  data:   "/var/lib/metricbeat"
  logs:   "/var/log/metricbeat"

### Modules Options ###
metricbeat.modules:
- module: apache
  metricsets: ["status"]
  enabled: true
  period: 10s
  hosts: ["http://localhost:8088/server-status"]
  tags: ["dynafed", "metrics", "httpd"]

### Outputs ###
output:
  logstash:
    enabled: true
    hosts:
      - "logstash-host:5044"
    index: "metricbeat"
    ssl:
      enabled: true
      verification_mode: full
      certificate_authorities: ["/etc/ssl/certs/ca-bundle.crt"]

### Logging Options ###
logging:
  level: "warning"
  to_files: true
  to_syslog: false
  files:
    path: "/var/log/metricbeat"
    name: "metricbeat.log"
    rotateeverybytes: 104857600
    keepfiles: 7
```

6.- [Logstash](roles/logstash): Can be run on the same server, or a remote one, but with good latency and network uptime. This is where the heavy lifting occurs as it needs to perform the following:


1. Parse the the log information and organize it in JSON object fields.
2. Aggregate the log lines for a single event when they are created on both the access and error logs. Usually the request method and its status code is on the access file, while the redirection and error information is located on the error file.
3. Analyze the event methods and their return statuscodes to determine the type of the transaction and whether it was successful or it failed.
4. Enrich and format the produced event documents, (resolving DNS, obtaining GeoIP information and renaming fields for easier reading and manipulation).
5. Extract the storage endpoint status into one document per endpoint.
6. Add field calculating the ration of used workers.
7. Output to Elasticsearch DB.

To do so:
1. [Custom grok filter](roles/logstash/patterns/ugr.grok) is installed on /etc/logstash/conf.d/patterns/
2. [15-beat-compat.conf](roles/logstash/files/filters/15-beat-compat.conf) on /etc/logstash/conf.d/
3. [20-dynafed-grok.conf](roles/logstash/files/filters/20-dynafed-grok.conf) on /etc/logstash/conf.d/
4. [22-dynafed-agg-1.conf](roles/logstash/files/filters/22-dynafed-agg-1.conf) is copied onto /etc/logstash/conf.d/
5. [26-dynafed-events.conf](roles/logstash/files/filters/26-dynafed-events.conf) is copied onto /etc/logstash/conf.d/
6. [28-dynafed-format.conf](roles/logstash/files/filters/28-dynafed-format.conf) is copied onto /etc/logstash/conf.d/
7. [30-dynafed-ugr-sestatus.conf](roles/logstash/files/filters/30-dynafed-ugr-sestatus.conf) is copied onto /etc/logstash/conf.d/
8. [40-dynafed-metrics-httpd.conf](roles/logstash/files/filters/40-dynafed-metrics-httpd.conf)  is copied onto /etc/logstash/conf.d/
9. [49-dynafed-logs-httpd-blacklist.conf](roles/logstash/files/filters/49-dynafed-logs-httpd-blacklist.conf)  is copied onto /etc/logstash/conf.d/
10. [50-dynafed-external-es-output.conf](roles/logstash/files/filters/50-dynafed-external-es-output.conf) is copied onto /etc/logstash/conf.d/ ***Make sure to set the proper elsaticsearch server(s), also add username/password and SSL options if required.**
