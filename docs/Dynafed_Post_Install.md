# Post Installation Setup

## Checksusm

The default checksum script will perorom the following:

1. curls the file into the dynafed storage
2. generates the checksum
3. outputs checksum string that will be captured by Dynafed.

It should generaly work, but there is a bottleneck having to download the files.

[ugrextchecksum.sh](../roles/dynafed/files/libexec/ugrextchecksum.sh)

For a more complicated script that performs the following:

1. ssh to remote storage endpoint
2. runs gfal-sum on the file
3. outputs checksum string that will be captured by Dynafed.

This makes sense if the user that ssh's into has access to the flat file on storage.

#### For TRIUMF this is how we set this up:

##### 1. Setup remote access

At Dynafed hosts:

1. Create an ssh key for each of the dynafed nodes without passphrase.
3. Apache needs to be able to use its key, so copy the keys to:

```sh
/usr/share/httpd/.ssh
```

with these permissions

```sh
share/httpd/.ssh» ls -la
total 12
drwxr-xr-x 2 root   root   57 Oct 21  2019 .
drwxr-xr-x 6 root   root   59 Apr  2 13:14 ..
-rw------- 1 apache root 1675 Oct 21  2019 id_rsa
-rw-r--r-- 1 apache root  420 Oct 21  2019 id_rsa.pub
-rw-r--r-- 1 apache root  400 Oct 21  2019 known_hosts
```

4. su to the apache user and ssh to the storage endpoint to create the .ssh/known_hosts file.

At the storage endpoint:

1. Create a dynafed user
2. Give user read permissions to the files.
3. Test that the user can gfal-sum against the files.
4. Add the public keys from the Dynafed hosts into the user file:

```
/home/dynafed/.ssh/authorized_keys
```

#### 2. Install checksum script

Install the script:

[ugrextchecksum_ssh_triumf_minio.py](../roles/dynafed/files/libexec/ugrextchecksum_ssh_minio.py)

at:

```sh
/usr/libexec/ugr/ugrextchecksum_ssh_triumf_minio.py
```

Configure Dynafed to use this script by changing this for each of the endpoints that need to:

```
locplugin.ENPOINT.checksumcalc: /usr/libexec/ugr/ugrextchecksum_ssh_triumf_minio.py /var/lib/minio /etc/ugr/conf.d/endpoints.conf
```

Also do this on every host to setup the log folders and permissions so the apache user can run the script and create the log files.

```sh
usermod -G dynafed apache
chown root:dynafed -R /usr/libexec/ugr/
chmod 755 /usr/libexec/ugr/ugrextchecksum_ssh_triumf_minio.py
chown -R root:dynafed /var/log/dynafed_storagestats
chmod g+w /var/log/dynafed_storagestats
systemctl restart httpd
```


## Third Party Copy

The defualt third party scripts streams the transfer through the dynafed. Therefore bandwith is an issue. With the proper script these can be offloaded.

We have come up with ways to offload it to other locations, but well, we’ll have to figure out the best way to do so for each location.

First of all, we need to configure gfal to default to streamed mode only.

In:

```
/etc/gfal2.d/http_plugin.conf
```

Check the following settings are:

```
[HTTP PLUGIN]
ENABLE_REMOTE_COPY=false
ENABLE_STREAM_COPY=true
ENABLE_FALLBACK_TPC_COPY=true
DEFAULT_COPY_MODE=streamed
```

Since there are two modes, PULL and PUSH, we need to setup two scripts.

#### Pull

The default script that Dynafed installs doesn't work out of the box. At least need to change this to the following.

[ugrpullscript_gfal.sh](../roles/dynafed/files/libexec/ugrpullscript_gfal.sh)

It will create a log file, /var/log/ugr/gfal2-tpc-pull.log so we need to make the directory:

```sh
mkdir /var/log/ugr
```

#### Push

The default script that Dynafed installs doesn't work out of the box. At least need to change this to the following.

[ugrpushscript_gfal.sh](../roles/dynafed/files/libexec/ugrpushscript_gfal.sh)


It will create a log file, /var/log/ugr/gfal2-tpc-pull.log so we need to make the directory:

```sh
mkdir /var/log/ugr
```

## Rucio File List Dump for Consistency Checks.

The purpose is to create a list of files behind each mount point from all endpoints behind each. The reason for this is that Rucio does checks every week or so to compare what it thinks each of its Rucio Endpoints has and what they actually have, and make adjustments.

https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DDMDarkDataAndLostFiles

The dynafed-storage command can do this for endpoints that support recursively lisitng the contents.
Only S3 has been tested extensively.

For Rucio we can use the --rucio flag to create the proper document.

```sh
dynafed-storage reports filelist --logfile $LOG_FILE --loglevel=WARNING -o $DUMPS_FOLDER --rucio -e $ep
```

It will generate a text file with the list of all available files, nanmed after the endpoint.
If there are several endpont behind a dynafed mountpoint, those files will need to be merged.

Generally these mountpoints are datadisk and scratchdisk. So in de end we should have two files.

Now those files need to be uploaded so that rucio can get them.
which would be in /atlas/datadisk/dumps and /atlas/scratchdisk/dumps respectively.

At TRIUMF and UVIC, we have a script that takes care of this in our Proxy server under /etc/cron.weekly
It’s kind of ugly since I never went back to improve it since it worked. But I’ll put it here as an example,

This uses S3 endpoints and so we use the aws client package to upload the resulting file and the dynafed_storagestats package.

```sh
pip3 install dynafed_storagestats
yum install awscli
```

The following example only works if the buckets mounted on the same path are in the same S3 endpoint.

To make weekly dumps, install it at:

```
/etc/cron.weekly/rucio_filedumps.sh
```

```sh
#!/bin/bash
DUMPS_FOLDER=/tmp/dynafed_file_dumps
LOG_FILE=/var/log/dynafed_storagestats/dynafed_reports.log
UGR_EP_CONFIG_FOLDER=/etc/ugr/conf.d
YESTERDAY=$(date -d "yesterday 00:00" '+%Y%m%d')
#ATLASDATADISK VARS
ATLASDATADISK_ENDPOINTS=(TRIUMF-S3-ATLASDATADISK01)
ATLASDATADISK_ENDPOINT_URL="http://atlas-fed-se1.triumf.ca:9002"
ATLASDATADISKADMIN_BUCKET="s3://triumf-s3-atlasdatadisk-admin"
ATLASDATADISK_ACCESS_KEY=""
ATLASDATADISK_SECRET_KEY=""
#ATLASSCRATCHDISK VARS
ATLASSCRATCHDISK_ENDPOINTS=(TRIUMF-S3-ATLASSCRATCHDISK02)
ATLASSCRATCHDISK_ENDPOINT_URL="http://atlas-fed-se1.triumf.ca:9000"
ATLASSCRATCHDISK_BUCKET="s3://triumf-s3-atlasscratchdisk-admin"
ATLASSCRATCHDISK_ACCESS_KEY=""
ATLASSCRATCHDISK_SECRET_KEY=""
#####
mkdir -p $DUMPS_FOLDER
cd $DUMPS_FOLDER
rm $DUMPS_FOLDER/atlasdatadisk_$YESTERDAY
for ep in $ATLASDATADISK_ENDPOINTS; do
  dynafed-storage reports filelist --logfile $LOG_FILE --loglevel=WARNING -o $DUMPS_FOLDER --rucio -e $ep
  cat $DUMPS_FOLDER/$ep.filelist >> $DUMPS_FOLDER/atlasdatadisk_$YESTERDAY
  rm $DUMPS_FOLDER/$ep.filelist
done
echo "Uploading file atlasdatadisk_$YESTERDAY"
export AWS_ACCESS_KEY_ID=$ATLASDATADISK_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=$ATLASDATADISK_SECRET_KEY
aws --endpoint-url $ATLASDATADISK_ENDPOINT_URL s3 cp /$DUMPS_FOLDER/atlasdatadisk_$YESTERDAY $ATLASDATADISKADMIN_BUCKET
rm $DUMPS_FOLDER/atlasscratchdisk_$YESTERDAY
for ep in $ATLASSCRATCHDISK_ENDPOINTS; do
  dynafed-storage reports filelist --logfile $LOG_FILE --loglevel=WARNING -o $DUMPS_FOLDER --delta 1 -p rucio -e $ep
  cat $DUMPS_FOLDER/$ep.filelist >> $DUMPS_FOLDER/atlasscratchdisk_$YESTERDAY
  rm $DUMPS_FOLDER/$ep.filelist
done
echo "Uploading file atlasscratchdisk_$YESTERDAY"
export AWS_ACCESS_KEY_ID=$ATLASSCRATCHDISK_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=$ATLASSCRATCHDISK_SECRET_KEY
aws --endpoint-url $ATLASSCRATCHDISK_ENDPOINT_URL s3 cp /$DUMPS_FOLDER/atlasscratchdisk_$YESTERDAY $ATLASSCRATCHDISK_BUCKET
```

To keep things separate, we have two buckets outside of the ones that hold the files called the admin buckets.

ATLASDATADISKADMIN_BUCKET for example

The thinking behind this was that there could be many endpoints behind datadisk for example. So we would need to gather file list dumps for each of them, merge them, and then upload them to the /dumps folder.

Those admin buckets are mounted on those /dumps folder. For example:

```
glb.locplugin[]: /usr/lib64/ugr/libugrlocplugin_s3.so TRIUMF-ATLASSCRATCHDISK-ADMIN 15 http://atlas-fed-se1.triumf.ca:9000/triumf-s3-atlasscratchdisk-admin
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.s3.pub_key:
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.s3.priv_key:
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.s3.alternate: yes
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.s3.signature_ver: s3v4
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.s3.region: us-east-1
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.xlatepfx: /atlas/scratchdisk/dumps /
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.writable: true
locplugin.TRIUMF-ATLASSCRATCHDISK-ADMIN.ssl_check: false
glb.locplugin[]: /usr/lib64/ugr/libugrlocplugin_s3.so TRIUMF-ATLASDATADISK-ADMIN 15 http://atlas-fed-se1.triumf.ca:9000/triumf-s3-atlasdatadisk-admin
locplugin.TRIUMF-ATLASDATADISK-ADMIN.s3.pub_key:
locplugin.TRIUMF-ATLASDATADISK-ADMIN.s3.priv_key:
locplugin.TRIUMF-ATLASDATADISK-ADMIN.s3.alternate: yes
locplugin.TRIUMF-ATLASDATADISK-ADMIN.s3.signature_ver: s3v4
locplugin.TRIUMF-ATLASDATADISK-ADMIN.s3.region: us-east-1
locplugin.TRIUMF-ATLASDATADISK-ADMIN.xlatepfx: /atlas/datadisk/dumps /
locplugin.TRIUMF-ATLASDATADISK-ADMIN.writable: true
locplugin.TRIUMF-ATLASDATADISK-ADMIN.ssl_check: false
```

That way, those file are always in the same location easily accessible to the Dynafed admins, regardless of what other locations are setup, and whether they use dcache or what not instead of S3 (and in the non-S3 cases they would need to provide the list to the dynafed admin somehow, so that we can then put the whole thing together)

And the variabler in the script like ATLASDATADISK_ENDPOINTS=()  We put a the names of all the endpoints we can do dynafed-storage commands against and create a file list for each.
The question is, whether we will have admin buckets later on or not (we can use any of the current buckets as admin bucket I guess)
The thing is that the TRIUMF S3 enpoint might go away later this year. And I’m not sure we can get other bucket at CERN, we can always ask. Or if you guys are going to setup endponts at your location?

I actually under /etc/ugr/conf.d I have an admin.conf file with those endpoints I pasted

we need to do this only on one of the hosts. For ours I chose the proxy one, seems to make more sense and also in a way is more stable location, but it can really be done anywhare, as long as it is only one, no need to run this several times.

Ah, of course. pip3 install dynafed-storagestats should do the trick.

We will also need to create a schema file so that the space_usage.json file can be created.

This file needs to go to:

```
/etc/dynafed/wlcg-schema.yml
```

```yaml
storageservice:
  name: "dynafed-atlas.heprc.uvic.ca"
  datastores: []
  implementation: "dynafed"
  qualitylevel: "production"
  storageendpoints:
    - name: "https"
      interfacetype: "https"
      qualitylevel: "production"
      endpointurl: "https://dynafed-atlas.heprc.uvic.ca"
      assignedshares:
        - "all"
  storageshares:
    - name: "ATLASDATADISK"
      assignedendpoints:
        - "all"
      path:
        - "/dynafed/atlas/datadisk"
      servingstate: "open"
      vos:
        - "atlas/Role=production"
      dynafedendpoints:
        - "TRIUMF-S3-ATLASDATADISK01"
    - name: "ATLASSCRATCHDISK"
      assignedendpoints:
        - "all"
      path:
        - "/dynafed/atlas/scratchdisk"
      servingstate: "open"
      vos:
        - "atlas"
      dynafedendpoints:
        - "TRIUMF-S3-ATLASSCRATCHDISK02"
```



## Authentication

To make VOMS work, need to install the main package and any vo specific ones.
You can add them to this ansible var:
dynafed_grid_packages:
  - edg-mkgridmap
  - gridsite
  - wlcg-voms-atlas
  - wlcg-voms-dteam
  - wlcg-voms-ops
