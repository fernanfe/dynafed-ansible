---
# This example will install a single cluster Elasticsearch Server protected with
# Opendistro:
#
# - elasticsearch-oss 6.8.1 with
# - opendistroforelasticsearch-0.10.0
# - HTTP TLS. Will use the same certs as the Transport TLS.
# - http://localhost.localhost:9200 -> ES API
# - http://localhost.localhost:9300 -> Admin API
# - Basic authentication.
# - Default users, roles and role-mappings.
# - Removes test package certs.
# - Changes default users passwords (set them below)
# - Adds a snaphost folder.
# - Adds custom admin cert, key and ca.
#
# Requirements:
#   For admin certificate:
#     - admin.crt, admin.key and admin-ca.crt files located in
#       inventory/group_vars/elasticsearch_nodes/elk-certs/.
#     - Files need to be in PKCS8 format and preferably encrypted with ansible-vault.
#     - User certificate should have "CN=admin". See
#       opendistro_elastic_security_authcz_admin_dn to change/add other.
#
#   For http and transport certificates:
#     - {{ inventory_hostname }}.crt, {{ inventory_hostname }}, and ca.crt files located in
#       /host_vars/{{ inventory_hostname }}/elk-certs/
#     - Files need to be in PKCS8 format and preferably encrypted with ansible-vault.
#     - User certificate should have "CN=localhost.localhost". See
#       opendistro_elastic_security_nodes_dn to change/add other.

# Name of package to be installed
elasticsearch_pkg_to_install: opendistro
opendistro_elastic_pkg_ver: "0.10.0"
# It's imprtant to select the elasticsearch version that is compatible with the
# chosen opndistro version. See
# https://opendistro.github.io/for-elasticsearch-docs/version-history/
elasticsearch_pkg_ver: "6.8.1"


### Elasticsearch options ###
elasticsearch_config_cluster_name: "elasticsearch"
elasticsearch_config_network_http_host: "[_local_]"
elasticsearch_config_network_http_port: 9200

#elasticsearch_config_dicovery_zen_ping_unicast_hosts: '["elk","elk2"]'

elasticsearch_config_dicovery_zen_minimum_master_nodes: 1

### Snapshot Repositories ###
# Recommended, but if not looking to make data shapshots, just delete.
elasticsearch_config_path_repo: "[/var/lib/elasticsearch/snapshots]"

#### JVM Options ####
# Adjust according to your system.
elasticsearch_jvm_options_ms: 5g
elasticsearch_jvm_options_mx: 5g
elasticsearch_jvm_options_ss: 1m

#### OpenDistro Settings ####

# Certs: !!!! Keys must be in PKCS8 format !!!!
opendistro_elastic_security_nodes_dn:
  - "CN=localhost.localhost"

opendistro_elastic_security_authcz_admin_dn:
  - "CN=admin"

## Admin Client Certificate
opendistro_elastic_security_admin_cert_enabled: true

## HTTP SSL Settings
opendistro_elastic_security_ssl_http_enabled: false

## Change these for production! Use Ansible-Vault.
# Admin user password.
opendistro_elastic_security_internal_users_admin_pass: "admin"
# Logsatsh user password.
opendistro_elastic_security_internal_users_logstash_pass: "logstash"
# Kibanaserver user password.
opendistro_elastic_security_internal_users_kibanaserver_pass: "kibanaserver"
# Kibana Read-Only user password.
opendistro_elastic_security_internal_users_kibanaro_pass: "kibanaro"
# Read All user password.
opendistro_elastic_security_internal_users_readall_pass: "readall"
# Snapshot restore user password.
opendistro_elastic_security_internal_users_snapshotrestore_pass: "snapshotrestore"
